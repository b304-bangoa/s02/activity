package com.zuitt.wdc043_s2_a2;

import java.util.ArrayList;
import java.util.Arrays;

public class Friends {
    public static void main(String[] args) {

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));

        System.out.println("My friends are: " + friends);
    }
}
