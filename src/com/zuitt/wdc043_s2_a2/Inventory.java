package com.zuitt.wdc043_s2_a2;

import java.util.HashMap;

public class Inventory {
    public static void main(String[] args){

        HashMap<String, Integer> inventory = new HashMap<>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " +inventory);

    }
}
